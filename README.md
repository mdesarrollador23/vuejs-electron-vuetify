<div align="center" width=""100vw> <img src="https://i.ibb.co/jzQ2jVZ/Sin-t-tulo.png" /></div>

# ivicar

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
