import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import { es, en } from '../locale'
Vue.use(Vuetify);

export default new Vuetify({
    breakpoint: {
        scrollBarWidth: 16,
        thresholds: {
            xs: 600,
            sm: 960,
            md: 1280,
            lg: 1920,
        },
    },
    lang: {
        locales: { es, en },
        current: 'en',
        t: undefined
    },
    theme: {
        dark: false,
        default: 'light',
        disable: false,
        options: {
            cspNonce: undefined,
            customProperties: undefined,
            minifyTheme: undefined,
            themeCache: undefined,
        },
        themes: {
            light: {

                primary: '#1565C0',
                topprimary: '#0D47A1',
                secondary: '#B71C1C',
                accent: '#E4F4E4',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FB8C00',
            },
            dark: {
                primary: '#212121',
                topprimary: '#424242',
                secondary: '#B71C1C',
                accent: '#E4F4E4',
                error: '#FF5252',
                info: '#2196F3',
                success: '#4CAF50',
                warning: '#FB8C00',
            },
        },
    },
});
