import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  // Home 
  {
    path: '/',
    redirect: '/auth',
  },
  // Auth 
  {
    path: '/auth',
    redirect: '/auth/signin',
    name: 'Auth',
    meta: {
      requiresAuth: true,
    },
    component: () => import('../views/auth/layout/Layout.vue'),
    children: [
      {
        path: '/auth/signin',
        name: 'SignIn',
        component: () => import('../views/auth/pages/SignIn.vue'),
        meta: {
          title: 'Iniciar Session',
        },
      },
      {
        path: '/auth/resetpassword',
        name: 'ResetPassword',
        component: () => import('../views/auth/pages/ResetPassword.vue'),
        meta: {
          title: 'Restablecer la contraseña',
        },
      },
      {
        path: '/auth/checkpin',
        name: 'CheckPin',
        component: () => import('../views/auth/pages/CheckPin.vue'),
        meta: {
          title: 'Verificar PIN',
        },
      },
    ]
  },
  // Dashboard
  {
    path: '/dashboard',
    redirect: '/dashboard/home',
    name: 'Dashboard',
    meta: {
      requiresAuth: true,
    },
    component: () => import('../views/dashboard/layout/Layout.vue'),
    children: [
      {
        path: '/dashboard/home',
        name: 'Home',
        component: () => import('../views/dashboard/pages/Home.vue'),
        meta: {
          title: 'Escritorio',
        },
      },
      {
        path: '/dashboard/profile',
        name: 'Profile',
        component: () => import('../views/dashboard/pages/Profile.vue'),
        meta: {
          title: 'Perfil',
        },
      },
      {
        path: '/dashboard/users',
        name: 'Users',
        component: () => import('../views/dashboard/pages/Users.vue'),
        meta: {
          title: 'Usuarios',
        },
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
