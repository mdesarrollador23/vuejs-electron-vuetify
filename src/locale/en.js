// USA
export default {
    badge: 'Badge',
    close: 'Close',
    FOOTER: {
        title: 'System Status: All Good',
        copyright: 'Dashboard, Inc.',
        createBy: 'Dashboard created by Miguel De Jesus'
    }
}